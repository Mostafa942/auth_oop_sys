<?php

class Database
{
    public $pdo;

    const CREDENTIALS = [
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'db' => 'auth'
    ];

    public function connect()
    {
        if ($this->pdo === null) {
            try {
                $this->pdo = new PDO(
                    'mysql:host=' . self::CREDENTIALS['host'] . ';dbname=' . self::CREDENTIALS['db'] . ';',
                    self::CREDENTIALS['user'],
                    self::CREDENTIALS['pass']
                );
            } catch (PDOException $e) {
                $message = date('y-m-d H:i:s') . ': ' . $e->getMessage() . "\n";
                file_put_contents('error-pdo-connection.txt', $message, FILE_APPEND);
                die('site down');
            }
        }
        return $this->pdo;
    }
}
