<?php


class User
{
    protected $errors = [];

    public function setDI()
    {
        $db = new Database;
        return $db->connect();
    }

    public function ValidationRgister($fullName, $email, $password)
    {
        // validate User Name=>
        if (empty($fullName)) {
            $this->errors[] = 'please enter your user name';
        } elseif (!preg_match('/^[a-zA-Z]{3,}(?: [a-zA-Z]*){0,2}$/', $fullName)) {
            $this->errors[] = 'user name should start by capital letter';
        }

        // validate email=>
        if (empty($email)) {
            $this->errors[] = 'please enter your email';
        } elseif (!preg_match('/^[a-z0-9-_.]+@gmail.com$/', $email)) {
            $this->errors[] = 'your email isn\'t correct';
        }

        // validate password=>
        if (empty($password)) {
            $this->errors[] = 'please enter your password';
        } elseif (!preg_match('/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[\d])\S*$/', $password)) {
            $this->errors[] = 'not less than 8 characters';
        }

        // validate rePassword=>
        if (empty($_POST['rePassword'])) {
            $this->errors[] = 'please enter your password';
        } elseif (empty($password) != empty($_POST['rePassword'])) {
            $this->errors[] = 'rePassword must be the same';
        }

        // validate agree=>
        if (empty($_POST['terms'])) {
            $this->errors[] = 'must agree to the terms';
        }

        if (count($this->errors)) {
?>
            <ul>
                <?php
                for ($i = 0; $i < count($this->errors); $i++) :
                ?>
                    <li>
                        <?= $this->errors[$i];
                        ?>
                    </li>
                <?php
                endfor;
                ?>
            </ul>
<?php
        } else {
            $this->RegisterUser($fullName, $email, $password);
            $this->startSession($fullName);
        }
    }

    protected function RegisterUser($fullName, $email, $password)
    {
        $password = md5($password);

        $insql = "INSERT INTO `users`(`fullName`, `email`, `password`) VALUES ('$fullName', '$email', '$password')";

        $stm = $this->setDI()->prepare($insql);

        $chsql = "SELECT `email` FROM `users` WHERE `email` = '$email'";

        $stmch = $this->setDI()->prepare($chsql);

        $stmch->execute();

        $result = $stmch->fetchAll(PDO::FETCH_ASSOC);

        if ($email && $result) {
            echo 'email is exist';
        } else {
            $stm->execute();
        }
    }

    public function login($email, $password)
    {
        $password = md5($password);

        $chsql = "SELECT `email`, `password` FROM `users` where `email` = '$email'";

        $stmch = $this->setDI()->prepare($chsql);

        $stmch->execute();

        if ($stmch->rowCount()) {
            $result = $stmch->fetchAll(PDO::FETCH_ASSOC);
            if ($result[0]['password'] != $password) {
                echo 'wrong pass';
            } else {
                $this->startSession($email);
            }
        } else {
            echo 'Invalid acc';
        }
    }

    protected function startSession($user)
    {
        session_start();
        $_SESSION['user'] = $user;
        header('Location: home.php');
        exit();
    }

    public function logout()
    {
        session_start();
        unset($_SESSION['user']);
        header('Location: login.php');
    }
}
